﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Study_Guide
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        DataView CodeDisplay()
        {

            DataTable code_example = new DataTable();

            //Line Column
            DataColumn line_col = new DataColumn();

            //Code Column
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            //Defines line column
            line_col.ColumnName = "Line";
            line_col.DataType = System.Type.GetType("System.Int32");

            //Adds code data to line column
            code_example.Columns.Add(line_col);

            //Defines code column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            code_example.Columns.Add(code_col);

            List<string> cssExample = new List<string>(new string[] {
                ".div3 {",
                "~float: left;",
                "~width: 100px;",
                "~height: 50px;",
                "~margin: 10px;",
                "~border: 3px solid #73AD21;",
                "}",
                ".div4 {",
                "~border: 1px solid red;",
                "~clear: left;",
                "}"
        });
            //This loop is from Christine's example
            int i = 0;
            foreach (string code_line in cssExample)
            {
                coderow = code_example.NewRow();
                coderow[line_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                code_example.Rows.Add(coderow);
            }

            DataView codeoutput = new DataView(code_example);
            return codeoutput;

        }
        DataView MyCode()
        {

            DataTable my_code = new DataTable();

            //Line Column
            DataColumn line_col = new DataColumn();

            //Code Column
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            //Defines line column
            line_col.ColumnName = "Line";
            line_col.DataType = System.Type.GetType("System.Int32");

            //Adds code data to line column
            my_code.Columns.Add(line_col);

            //Defines code column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            my_code.Columns.Add(code_col);

            List<string> my_css = new List<string>(new string[] {
                ".sideleft {",
                "~float: left;",
                "~width: 18 %;",
                "~position: absolute;",
                "~background - color: #a8fbff;",
                "~margin: 5px;",
                "}",
                ".sideform {",
                "~width: 70%;",
                "~float: right;",
                "~background-color: #6e7f80;",
                "~font-weight: bold;",
                "}",
                "main {",
                "~width: 70%;",
                "~float: right;",
                "~background-color: #6e7f80;",
                "}",
                "#foot {",
                "~clear: both;",
                "~width: 100%;",
                "}"

            });
            //This loop is from Christine's example
            int i = 0;
            foreach (string code_line in my_css)
            {
                coderow = my_code.NewRow();
                coderow[line_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                my_code.Rows.Add(coderow);
            }

            DataView codeoutput = new DataView(my_code);
            return codeoutput;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataView cd = CodeDisplay();
            css_ex.DataSource = cd;
            css_ex.DataBind();

            DataView mc = MyCode();
            my_css.DataSource = mc;
            my_css.DataBind();
        }

        
    }
}