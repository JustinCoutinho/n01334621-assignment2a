﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="Study_Guide.usercontrols.CodeBox" %>

<div class="col-md-4 col-xm-12">

         <asp:DataGrid ID="cb1" runat="server" CssClass="code" GridLines="None" Width="300px" CellPadding="2">
         <HeaderStyle Font-Size="Large" BackColor="#800000" ForeColor="White" />
         <ItemStyle BackColor="#1a1d23" ForeColor="Orange" />
         </asp:DataGrid>

    <uc:CodeBox runat="server" id="cb1" OnInit="CodeBox_Init" />


</div>

<div class="col-md-8 col-xm-12">


     
         <asp:DataGrid ID="cb2" runat="server" CssClass="code" GridLines="None" Width="500px" CellPadding="2">
         <HeaderStyle Font-Size="Large" BackColor="#800000" ForeColor="White" />
         <ItemStyle BackColor="#1a1d23" ForeColor="Orange" />
         </asp:DataGrid>

    <uc:CodeBox runat="server" id="cb2" OnInit="CodeBox_Init" />

</div>

