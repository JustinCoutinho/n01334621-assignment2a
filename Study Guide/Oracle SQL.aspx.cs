﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Study_Guide
{
    public partial class Contact : Page
    {
        DataView CodeDisplay()
        {

            DataTable code_example = new DataTable();

            //Line Column
            DataColumn line_col = new DataColumn();

            //Code Column
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            //Defines line column
            line_col.ColumnName = "Line";
            line_col.DataType = System.Type.GetType("System.Int32");

            //Adds code data to line column
            code_example.Columns.Add(line_col);

            //Defines code column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            code_example.Columns.Add(code_col);

            List<string> sqlExample = new List<string>(new string[] {
             "SELECT student, assignment",
            "FROM assignments",
            "LEFT JOIN students",
            "ON assignments.class = students.class",

            "DATA",
            "Birinder  ||  Paper",
            "Amandeep  ||  Exam",
            "(null)    ||  Lab",
        });
            //This loop is from Christine's example
            int i = 0;
            foreach (string code_line in sqlExample)
            {
                coderow = code_example.NewRow();
                coderow[line_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                code_example.Rows.Add(coderow);
            }

            DataView codeoutput = new DataView(code_example);
            return codeoutput;

        }
        DataView MyCode()
        {

            DataTable my_code = new DataTable();

            //Line Column
            DataColumn line_col = new DataColumn();

            //Code Column
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            //Defines line column
            line_col.ColumnName = "Line";
            line_col.DataType = System.Type.GetType("System.Int32");

            //Adds code data to line column
            my_code.Columns.Add(line_col);

            //Defines code column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            my_code.Columns.Add(code_col);

            List<string> my_sql = new List<string>(new string[] {
            "SELECT invoice_id, v.vendor_name", 
            "FROM invoices i",
            "FULL JOIN vendors v",
            "ON v.vendor_id = i.vendor_id"
    
            });
            //This loop is from Christine's example
            int i = 0;
            foreach (string code_line in my_sql)
            {
                coderow = my_code.NewRow();
                coderow[line_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                my_code.Rows.Add(coderow);
            }

            DataView codeoutput = new DataView(my_code);
            return codeoutput;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataView cd = CodeDisplay();
            sql_ex.DataSource = cd;
            sql_ex.DataBind();

            DataView mc = MyCode();
            my_sql.DataSource = mc;
            my_sql.DataBind();
        }
    }
}