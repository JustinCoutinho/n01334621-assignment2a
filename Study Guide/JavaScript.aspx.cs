﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Study_Guide.usercontrols;

namespace Study_Guide
{
    public partial class _Default : Page
    {
        DataView CodeDisplay()
        {

            DataTable code_example = new DataTable();

            //Line Column
            DataColumn line_col = new DataColumn();

            //Code Column
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            //Defines line column
            line_col.ColumnName = "Line";
            line_col.DataType = System.Type.GetType("System.Int32");

            //Adds code data to line column
            code_example.Columns.Add(line_col);

            //Defines code column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            code_example.Columns.Add(code_col);

            List<string> jsExample = new List<string>(new string[] {
            "var cars = ['BMW', 'Volvo', 'Saab', 'Ford'];",
            "var i = 0;",
            "var text = ''; ",
            "~for (; cars[i];)",
            "~{",
            "~~text += cars[i] + '<br>';",
            "~~i++;",
            "~}",
        });
            //This loop is from Christine's example
            int i = 0;
            foreach (string code_line in jsExample)
            {
                coderow = code_example.NewRow();
                coderow[line_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                code_example.Rows.Add(coderow);
            }

            DataView codeoutput = new DataView(code_example);
            return codeoutput;
                        
        }
        DataView MyCode()
        {

            DataTable my_code = new DataTable();

            //Line Column
            DataColumn line_col = new DataColumn();

            //Code Column
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            //Defines line column
            line_col.ColumnName = "Line";
            line_col.DataType = System.Type.GetType("System.Int32");

            //Adds code data to line column
            my_code.Columns.Add(line_col);

            //Defines code column
            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            my_code.Columns.Add(code_col);

            List<string> my_javascript = new List<string>(new string[] {
            "var book1 = ' Ender's Game '",
            "var book2 = ' Catcher and the Rye '",
            "var book3 = ' A Clockwork Orange '",
            "var book4 = ' 1984 '",
            "var book5 = ' Game of Thrones '",
            "var book6 = ' Lord of the Rings '",
            "var book7 = ' The Hobbit '",
            "var book8 = ' It '",
            "var book9 = ' A Clash of Kings '",
            "var book10 = ' Norse Mythology '",
            "~//Assigning all book variables to an array",
            "var bookArray = [book1, book2, book3, book4, book5, book6, book7, book8, book9, book10];",
            "~//assigning a variable for the loop",
            "var i = 0;",
            "~//While loop runs as long as the condion is met",
            "while (i < bookArray.length)",
            "{ ~~//condition that must be met (input less than 10)",
            "~~console.log(' Book ' + (i + 1) + ':' + bookArray[i]); //sends the book number and title to the console",
            "~~i++; //incrementor meaning we are adding to something",
            "}",
            "~/*for(var i = 0; i < bookArray.length; i++) {",
            "~console.log('Book ' + (i + 1) + ':' + bookArray[i]);",
            "~} */",
            "~~//Prompts question",
            "var userInput = prompt('What top 10 book would you like?', 'Pick a number: 1-10');",
            "~//Input validation logic (Only accepts numbers between 1 and 10)",
            "if (userInput > 10 || userInput < 1 || userInput === '' || userInput === 'Pick a number: 1-10' || userInput === isNaN)",
            "{",
            "~~alert('Please enter a number between 1 and 10');",
            "}",
            "else",
            "{",
            "~~alert('Book ' + userInput + ':' + bookArray[userInput - 1]); //Runs if conditions are met, subtracting 1 from userinput",
            "}"
            });
            //This loop is from Christine's example
            int i = 0;
            foreach (string code_line in my_javascript)
            {
                coderow = my_code.NewRow();
                coderow[line_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                my_code.Rows.Add(coderow);
            }

            DataView codeoutput = new DataView(my_code);
            return codeoutput;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //DataView cd = CodeDisplay();
            //js_ex.DataSource = cd;
            //js_ex.DataBind();

            //DataView mc = MyCode();
            //my_js.DataSource = mc;
            //my_js.DataBind();
        }
        //https://forums.asp.net/t/2139408.aspx?access+a+dropdownlist+value+from+a+web+user+control 
        protected override Codebox OnInit(EventArgs e)
        {
            base.OnInit(e);

            CodeBox.Controls.Add(createInstance("code", "owner"));
        }
    }
}