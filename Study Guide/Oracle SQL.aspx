﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Oracle SQL.aspx.cs" Inherits="Study_Guide.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Oracle SQL</h1>
        <p>Oracle SQL is a useful language for pulling and arranging data, as well as creating tables. One of the most difficult things I have learned in SQL is
        joins. The concept is not that difficult, however, knowing how and when to use a type of join is more tricky. For example, left and right joins are best
        used when you want to compare data between two tables and see the data that does not match. A full join is best used when you want to merge the data that
        is common between two tables.
        </p>
    </div>

</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent2" runat="server">
     <h2>Example</h2>   
    <asp:DataGrid ID="sql_ex" runat="server" CssClass="code" GridLines="None" Width="300px" CellPadding="2">
         <HeaderStyle Font-Size="Large" BackColor="#800000" ForeColor="White" />
         <ItemStyle BackColor="#1a1d23" ForeColor="Orange" />
     </asp:DataGrid>

</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent3" runat="server">
<h3>My Code</h3>
    <asp:DataGrid ID="my_sql" runat="server" CssClass="code" GridLines="None" Width="500px" CellPadding="2">
         <HeaderStyle Font-Size="Large" BackColor="#800000" ForeColor="White" />
         <ItemStyle BackColor="#1a1d23" ForeColor="Orange" />
     </asp:DataGrid>

</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent4" runat="server">
<div class="row">
<h4>Description</h4>
    <p> 
    My Code is an example of a full join. Here, we are selecting the invoice ids and vender names from the invoices table and vendors table.
    In order to do this, we must full join the vendors table. This will join all data similarities in these columns. For this, I used alaises i and v to distinguish
    which columns are coming from vendors and invoices. We joined on vendor id because both tables have that column. This query will output the invoice id and
    vendor name of the vendor ids that are common between the two tables.
    </p>
</div>
</asp:Content>
