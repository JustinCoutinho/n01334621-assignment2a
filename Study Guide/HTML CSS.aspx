﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTML CSS.aspx.cs" Inherits="Study_Guide.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="css" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
    <h1>Cascading Style Sheets (CSS)</h1>
        <p>CSS is pretty straightforward conceptually, however, once you start practising it, you realize it is not as easy as it looks.
            Floats and clear are both extremely useful tools to use when you are styling a page that is not cooperating with your vision. In fact, I
            cannot imagine styling a page without them. The float property specifies where an element should go.
            Clear is a helpful property when you have multiple float properties and they are interfering with other elements on the page. Clear specifies
            what elements can float beside it. For example, "clear: left" clears an element that is to the left.
        </p>
    </div>
</asp:Content>
<asp:Content ID="example_code" ContentPlaceHolderID="MainContent2" runat="server">

    <h2>Example</h2>

    <asp:DataGrid ID="css_ex" runat="server" CssClass="code"
        GridLines="None" Width="300px" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#800000" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="Orange"/>     
    </asp:DataGrid>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent3" runat="server">

<h3>My Code</h3>

<asp:DataGrid ID="my_css" runat="server" CssClass="code"
        GridLines="None" Width="500px" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#800000" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="Orange"/>     
    </asp:DataGrid>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent4" runat="server">

<h4>Description</h4>
    <p>
    What my code demonstrates is the use of floats and clears in CSS. There are multiple elements floating left and right.
    I had to use "clear:both" in the footer to push it to the bottom of the page. Otherwise, it was overlapping with
    the other content because the floats in place altered the original layout of the page. This establishes that both
    sides around the footer should have nothing beside it.
    </p>
</asp:Content>
